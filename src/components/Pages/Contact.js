import React, {Component} from 'react';
import Field from '../Common/Field';
import {withFormik} from 'formik';
import * as Yup from 'yup';

const fields = {
	sections: [
		[
			{name:'name', elementName: 'input', type: 'text', placeholder:'Your name*'},
			{name:'email', elementName: 'input', type: 'email', placeholder:'Your email*'},
			{name:'phone', elementName: 'input', type: 'text', placeholder:'Your phone number*'}
		],
		[	
			{name:'message', elementName: 'textarea', type: 'text', placeholder:'Type your message*'}
		]
	]
}

class Contact extends Component{

	render(){
		return(
			<section className="page-section" id="contact">
	            <div className="container">
	                <div className="text-center">
	                    <h2 className="section-heading text-uppercase">Contact Us</h2>
	                    <h3 className="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
	                </div>
	                <form id="contactForm" name="sentMessage" novalidate="novalidate" onSubmit={this.props.handleSubmit}>
	                    <div className="row align-items-stretch mb-5">
	                   		
	                   		{fields.sections.map((section, sectionIndex) => {
	                   			console.log("Rendering section", sectionIndex, "with", section);
	                   			return (
	                   				<div className="col-md-6" key={sectionIndex}>
	                   					{section.map((field, i) => {
	                   						return <Field 
	                   									{...field} 
	                   									key={i} 
	                									value={this.props.values[field.name]}
	                									name={field.name}
	                									onChange={this.props.handleChange}
	                									onBlur={this.props.handleBlur}
	                									touched={(this.props.touched[field.name])}
	                									errors={this.props.errors[field.name]}

	                   								/>
	                   					})}
	                   				</div>
	                   			)
	                   		})}
	                    </div>
	                    <div className="text-center">
	                        <div id="success"></div>
	                        <button 
	                        className="btn btn-primary btn-xl text-uppercase" 
	                        id="sendMessageButton" 
	                        type="submit"
	                        // onClick={e => this.submitForm(e)}
	                        >Send Message</button>
	                    </div>
	                </form>
	            </div>
	        </section>
		)
	}
}

export default withFormik({
	//3 main things we need here
	mapPropsToValues: () => ({
		name: '',
		email: '',
		phone: '',
		message: '',
	}),
	//creating a new property with Yup:

	validationSchema: Yup.object().shape({
		name: Yup.string()
		.min(3, 'Not long enough')
		.required('You must give us your name.'),
		email: Yup.string()
		.email('You must enter a valid email address.')
		.required('We need your email.'),
		phone: Yup.string()
		.min(10, 'Not long enough')
		.max(15, 'Too long')
		.required('We need your phone number'),
		message: Yup.string()
		.min(500, 'You need to provide a longer message')
		.required('Message is required')
	}),

	handleSubmit: (values, {setSubmitting}) => {
		console.log("VALUES", values);
		alert("You've submitted the form.", JSON.stringify(values));
	}

})(Contact);